import pygmsh
import numpy as np
from scipy.sparse import coo_array
import scipy.linalg as spla

#model parameters
# WIDTH = 10.0, HEIGHT = 8.0, DEPTH = 2.0, mesh_size_param = 0.3

class ModelDomain:
    def __init__(self, WIDTH = 10.0, HEIGHT = 8.0, DEPTH = 2.0, mesh_size_param = 0.2, mesh_to_load=None):
        if mesh_to_load is not None:
            self.mesh = mesh_to_load
        else:
            self.WIDTH = WIDTH
            self.HEIGHT = HEIGHT
            self.DEPTH = DEPTH
            self.mesh_size_param = mesh_size_param
            self.dirichlet_vals = np.array([0, 0, 5, 0, 3, 10, 3])
            self.eps0 = 8.854187e-12
            with pygmsh.occ.geometry.Geometry() as geom:
                outer_rect = geom.add_rectangle([0.0, 0.0, 0.0], WIDTH, HEIGHT)
                circle1 = geom.add_disk([WIDTH*(1.0/3.0), HEIGHT*(2.0/3.0), 0.0], min(1.0/7.0*HEIGHT, 1.0/8.0*WIDTH))
                circle2 = geom.add_disk([WIDTH*(2.0/3.0), HEIGHT*(2.0/3.0), 0.0], min(1.0/7.0*HEIGHT, 1.0/8.0*WIDTH))
                inner_rect = geom.add_rectangle([1.0/3.0*WIDTH, 2.0/9.0*HEIGHT, 0.0], 1.0/3.0*WIDTH,  1.0/9.0*HEIGHT)
                flat = geom.boolean_difference(outer_rect, geom.boolean_union([circle1, circle2, inner_rect]))
                geom.extrude(flat, [0, 0, DEPTH])

                geom.set_mesh_size_callback(lambda *_: mesh_size_param)
                mesh = geom.generate_mesh()
            
            self.mesh = mesh
        self.mark_boundary_domains()
        self.eps = np.ones(self.n_var_pts) * self.eps0
        self.get_tetra_vols()
        self.initialize_matrix()
        
    def mark_boundary_domains(self):
        self.boundary_domains = np.zeros(len(self.mesh.points), dtype=np.int8)
        for face in self.mesh.cells[1].data:
            points = np.array(self.mesh.points[face])
            # normal = np.cross(points[2,:]-points[0,:], points[1,:]-points[0,:])

            # condition for faces lying on the top/bottom or front/back outer surface (no dirichlet boundary)
            if max(points[:,2]) < 1e-8 or min(points[:,2]) > self.DEPTH-1e-8 or max(points[:,1]) < 1e-8 or min(points[:,1]) > self.HEIGHT-1e-8: 
                self.boundary_domains[face] = np.maximum(self.boundary_domains[face], 1)
            # condition for faces lying on the left/right outer surface (dirichlet boundary of the main contacts)
            elif max(points[:,0]) < 1e-8:
                self.boundary_domains[face] = 2
            elif min(points[:,0]) > self.WIDTH-1e-8:
                self.boundary_domains[face] = 3
            # condition for faces inside the "mouth"
            elif max(points[:,1]) < self.HEIGHT/2.0:
                self.boundary_domains[face] = 4
            # condition for faces inside the "left eye"            
            elif max(points[:,0]) < self.WIDTH/2.0:
                self.boundary_domains[face] = 5
            # only the "right eye" is left
            else: 
                self.boundary_domains[face] = 6
        # self.variable_pts = np.zeros(np.count_nonzero(self.boundary_domains <= 1), dtype=np.int32)
        self.variable_pts = np.zeros(self.mesh.points.shape[0])
        var_idx = 0
        for point_idx in range(len(self.mesh.points)):
            if self.boundary_domains[point_idx] <= 1:
                self.variable_pts[point_idx] = var_idx
                var_idx += 1
            else:
                self.variable_pts[point_idx] = -1
        self.n_var_pts = var_idx
        # print(var_idx)
        # print(np.count_nonzero(self.variable_pts != -1))

    def get_tetra_vols(self):
        self.tetra_vols = np.zeros(len(self.mesh.cells[2].data))
        for idx, cell in enumerate(self.mesh.cells[2].data):
            coord_mat = np.column_stack((np.ones(4), self.mesh.points[cell]))
            self.tetra_vols[idx] = 1.0/6.0 * spla.det(coord_mat)
            assert(self.tetra_vols[idx] > 0)

    def initialize_matrix(self):
        self.dirichlet_term = np.zeros(self.n_var_pts)
        data = np.zeros(16*self.mesh.cells[2].data.shape[0])
        rows = np.zeros(16*self.mesh.cells[2].data.shape[0])
        cols = np.zeros(16*self.mesh.cells[2].data.shape[0])

        for idx, cell in enumerate(self.mesh.cells[2].data):
            coord_mat = np.column_stack((np.ones(4), self.mesh.points[cell]))
            inv_coord_mat = spla.inv(coord_mat)
            for co1 in range(4):
                for co2 in range(co1,4):
                    if self.variable_pts[cell[co2]] == -1 and self.variable_pts[cell[co1]] == -1:
                        continue
                    # grad_prod = self.eps0 * np.dot(inv_coord_mat[1:,co1], inv_coord_mat[1:,co2]) * self.tetra_vols[idx]
                    grad_prod = np.dot(inv_coord_mat[1:,co1], inv_coord_mat[1:,co2]) * self.tetra_vols[idx]
                    if self.variable_pts[cell[co1]] == -1:
                        self.dirichlet_term[int(self.variable_pts[cell[co2]])] -= self.dirichlet_vals[self.boundary_domains[cell[co1]]] * grad_prod * self.eps[int(self.variable_pts[cell[co2]])]
                        continue
                    if self.variable_pts[cell[co2]] == -1:
                        self.dirichlet_term[int(self.variable_pts[cell[co1]])] -= self.dirichlet_vals[self.boundary_domains[cell[co2]]] * grad_prod * self.eps[int(self.variable_pts[cell[co1]])]
                        continue
                    data[16*idx + 4*co1 + co2] = grad_prod * self.eps[int(self.variable_pts[cell[co2]])]#  * self.tetra_vols[idx]
                    rows[16*idx + 4*co1 + co2] = self.variable_pts[cell[co1]]
                    cols[16*idx + 4*co1 + co2] = self.variable_pts[cell[co2]]
                    if co1 != co2:
                        data[16*idx + 4*co2 + co1] = grad_prod * self.eps[int(self.variable_pts[cell[co1]])]#  * self.tetra_vols[idx]
                        rows[16*idx + 4*co2 + co1] = self.variable_pts[cell[co2]]
                        cols[16*idx + 4*co2 + co1] = self.variable_pts[cell[co1]]
        
        self.stiffness_mat = coo_array((data, (rows, cols)), shape=(self.n_var_pts, self.n_var_pts))
        
        
