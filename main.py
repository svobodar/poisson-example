import pyvista as pv
import numpy as np
from generate_mesh import ModelDomain
from scipy.sparse.linalg import spsolve
    
mesh = ModelDomain()

pv_mesh = pv.wrap(mesh.mesh)
# pv_mesh['values'] = np.sin(pv_mesh.points[:,0] + 2*pv_mesh.points[:,1] + 3*pv_mesh.points[:,2])
# pv_mesh['values'] = mesh.boundary_domains

rho_vec = np.zeros(mesh.n_var_pts)
# for i in range(mesh.mesh.points.shape[0]):
#     if mesh.variable_pts[i] == -1:
#         continue
#     rho_vec[int(mesh.variable_pts[i])] = mesh.mesh.points[i][2] * 1.60218e-19 * 1e7
rho_vec += mesh.dirichlet_term
print(max(mesh.dirichlet_term))

sol_vec = spsolve(mesh.stiffness_mat.tocsc(), rho_vec)
val_vec = np.zeros_like(mesh.variable_pts)
for i in range(mesh.mesh.points.shape[0]):
    if mesh.variable_pts[i] == -1:
        val_vec[i] = mesh.dirichlet_vals[mesh.boundary_domains[i]]
        continue
    val_vec[i] = sol_vec[int(mesh.variable_pts[i])]

pv_mesh['values'] = val_vec# np.minimum(val_vec, 20)

# pv_mesh.plot(scalars='values', show_edges=True, cmap=['black', 'blue', 'yellow', 'grey', 'red', 'green', 'black'])
pv_mesh.plot(scalars='values', show_edges = True)

print(mesh.variable_pts)
print(mesh.n_var_pts)


# coord_mat = np.column_stack((np.ones(4), mesh.mesh.points[mesh.mesh.cells[2].data[3]]))
# inv_coord_mat = np.linalg.inv(coord_mat)
# print(inv_coord_mat.T @ coord_mat[0,:])
